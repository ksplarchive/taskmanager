package com.task.ServletFile;

import java.io.IOException;
import java.sql.Connection;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.*;

import com.task.bizlogic.TaskBizlogic;
import com.task.dto.TaskDTO;
import com.task.util.DBUtil;
import com.task.util.Utility;

@WebServlet("/TaskReportMailServlet")
/**
 * Send Task details summary manually  
 * @author trojen
 *
 */

public class TaskReportMailServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
    private static Logger logger = Logger.getLogger(TaskReportMailServlet.class);
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		try 
		{	
			Calendar c = Calendar.getInstance();
			int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			if(dayOfWeek != Calendar.SUNDAY && dayOfWeek != Calendar.SATURDAY)
			{
			DBUtil dbUtil = DBUtil.getInstance();
			Connection connection = dbUtil.getConnection();
			
			TaskBizlogic bizlogic =new TaskBizlogic();
			bizlogic.sendTaskReport(connection);
			
			DBUtil.closeConnection(connection);
			logger.info("Sent all task details of employee.");
			}
			else
			{
				logger.info("Mail is Not Sent in week ends.");
			}
		}
		catch(Exception exception)
		{
			logger.error("Exception is occur during the sending report of Employees");
		}
	}
	
}
