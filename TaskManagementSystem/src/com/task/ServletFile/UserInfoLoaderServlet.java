package com.task.ServletFile;

import java.io.IOException;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import com.task.bizlogic.TaskBizlogic;
import com.task.dto.TaskDTO;
import com.task.util.DBUtil;
import com.task.util.Utility;

@WebServlet("/UserInfoLoaderServlet")
/**
 * Load data on web page when jsp page is called
 * load Employee Name , Date , previous task details of particular date.  
 * @author trojen
 *
 */
public class UserInfoLoaderServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(UserInfoLoaderServlet.class);
  	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, 
  	                                                                                      IOException 
  	{
  		int id =Integer.parseInt(request.getParameter("id"));
  		String date = request.getParameter("date");
  		
  		DBUtil dbUtil = DBUtil.getInstance();
		Connection connection = dbUtil.getConnection();
  		TaskBizlogic bizlogic = new TaskBizlogic();
  		String name = bizlogic.getEmployeeName(id,connection);
  		Utility utility = new Utility();
  		Date utilDate = utility.parseDate(date, "dd-MM-yyyy");
  		TaskDTO dto = bizlogic.getTaskDetailsByDate(id,utilDate, connection);
  		
  		DBUtil.closeConnection(connection);
  		
  		String currentTaskOfDate = "";
  		String nextTaskOfDate = "";
  		boolean exist = bizlogic.isExist(dto);
  	
  		if(exist)
  		{
  			currentTaskOfDate = dto.getTodaysTask();
  			nextTaskOfDate = dto.getTomorrowsTask();
  			exist = true;
  		}
  		
  		boolean isUpdatable = bizlogic.isUpdatable(utilDate);
  		Map<String, Object> map = new HashMap<String, Object>();
  	
  		try 
  		{	
  			map.put("name",name);
  			map.put("date",date);
  			map.put("id",id);
 			map.put("currentTask", currentTaskOfDate);
 			map.put("nextTask", nextTaskOfDate);	  			
  			map.put("exist", exist);
  			map.put("isUpdatable", isUpdatable);
  			
		} 
  		catch (Exception e) 
  		{	
  			logger.error(e.getMessage(),e);
		}

  		request.setAttribute("taskDetailsMap", map);
  		logger.info("sent data to jsp.");
  		RequestDispatcher dispatcher = request.getRequestDispatcher("/TaskManager.jsp");
  		dispatcher.forward(request, response);
  	}

}
