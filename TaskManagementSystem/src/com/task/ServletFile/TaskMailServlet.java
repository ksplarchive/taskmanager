package com.task.ServletFile;
import java.sql.Connection; 
import java.util.Calendar;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.task.bizlogic.TaskBizlogic;
import com.task.util.DBUtil;
import com.task.util.Utility;

@WebServlet("/TaskMailServlet")
/**
 * Send email to employee manually
 * @author trojen
 *
 */

public class TaskMailServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger("TaskMailServlet");
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
	{		
		try
		{
			Calendar c = Calendar.getInstance();
			int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			if(dayOfWeek != Calendar.SUNDAY && dayOfWeek != Calendar.FRIDAY)
			{
				DBUtil dbUtil = DBUtil.getInstance();
				Connection connection = dbUtil.getConnection();
				TaskBizlogic bizlogic = new TaskBizlogic();
				bizlogic.sendEmailToAll(connection);
				DBUtil.closeConnection(connection);
				logger.info("Sent mail to every employee.");
			}
			
		}
		catch(Exception e)
		{
			logger.error("Exception occuer At url sending time to Employee ! "+ e.getMessage(),e);
		}
	}
}
