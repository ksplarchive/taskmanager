package com.task.ServletFile;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.task.bizlogic.TaskBizlogic;
import com.task.dto.TaskDTO;
import com.task.util.Utility;
import com.task.util.DBUtil;

@WebServlet("/TaskStoreServlet")
 /**
  * Accept JSONObject from jsp file and store data into database
  * and if data is already exist then update it. 
  * @author trojen
  *
  */
public class TaskStoreServlet extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger("TaskStoreServlet");
	
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		try{
			
			 BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
			 String json = "";
			  
		     if(br != null)
		     {
		         json = br.readLine();
		     } 
		     
		     logger.info(json);
		     
		     JSONObject jsonObj = new JSONObject(json);
	         String strId = (String) jsonObj.get("id");
	         String date = (String)jsonObj.get("date");
   	         String todaysTask = (String)jsonObj.get("current_task");
		     String tomorrowsTask = (String)jsonObj.get("next_task");
		     String status = (String)jsonObj.get("status");
		     TaskDTO taskDTO = new TaskDTO();
		     int id = Integer.parseInt(strId);
		     taskDTO.setID((id));
		     taskDTO.setTodaysTask(todaysTask);
		     taskDTO.setTomorrowsTask(tomorrowsTask);
			
		     Utility utility = new Utility();
		     Date utilDate = utility.parseDate(date,"dd-MM-yyyy");
		     taskDTO.setDate(utilDate);
			
		     DBUtil dbUtil = DBUtil.getInstance();
		     Connection connection = dbUtil.getConnection();
		     TaskBizlogic bizlogic = new TaskBizlogic();
		     
		     if(status.equals("insert"))
		     {
		    	 bizlogic.saveTaskDetails(taskDTO,connection);
		    	 logger.info("Details Inserted.");
		     }
		     else
		     {
		    	 bizlogic.updateTaskDetails(taskDTO, connection);
		    	 logger.info("Details Updated.");
		     }
		     
		     DBUtil.closeConnection(connection);
		     logger.info("Connection was Closed.");
			 logger.info("Save Data into database."); 
		}
		catch (Exception e) {
			
			logger.error("Exception during the  execution of TaskStoreServlet"+ e.getMessage(),e);
		}
		
	}

}
