package com.task.dto;

import java.util.Date;

/**
 * /**
 * Data Transfer Object
 * @author trojen
 *
 */

public class TaskDTO 
{
	int id;
	String name;
	String todaysTask="Not Provided";
	String tomorrowsTask="Not Provided";
	Date date;
	
	public void setName(String name) 
	{
		this.name=name;
	}
	
	public void setTodaysTask(String todaysTask) 
	{
		this.todaysTask=todaysTask;	
	}
	
	public void setTomorrowsTask(String tomorrowsTask)
	{
		this.tomorrowsTask=tomorrowsTask;		
	}
	
	public  void setID(int id)
	{
		this.id=id;
	}
	
	public void setDate(Date date)
	{		
		this.date=date;
	}
	
	public Date getDate()
	{
		return date;
	}
	
	public int getId() 
	{
		return id;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getTodaysTask() 
	{
		return todaysTask;
	}
	
	public String getTomorrowsTask() 
	{
		return tomorrowsTask;
	}
	
}
