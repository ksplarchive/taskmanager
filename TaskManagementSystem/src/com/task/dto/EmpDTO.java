package com.task.dto;

import java.text.SimpleDateFormat;

import java.util.Date;

/**
 * Data Transfer Object
 * @author trojen
 *
 */

public class EmpDTO 
{
    String fullName;
	String email;
	Date date;
	int id;
	
	public void setID(int id)
	{
		this.id=id;
	}
	public void setName(String fullName) 
	{
		this.fullName=fullName;
	}

	public void setEmail(String email)
	{
		this.email=email;
	}
	
	public int getId() 
	{
		return id;
	}

	public String getName() 
	{
		return fullName;
	}

	public String getEmail()
	{
		return email;
	}
	
	public String getDate()  
	{
		return new SimpleDateFormat("dd-MM-yyyy").format(date);
	}

	public void setDate(Date currentDate)
	{
		this.date=currentDate;		
	}

}
