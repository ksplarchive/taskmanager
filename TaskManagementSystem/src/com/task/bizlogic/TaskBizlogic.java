package com.task.bizlogic;

import java.io.File; 
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.task.dao.TaskDAO;
import com.task.dto.EmpDTO;
import com.task.dto.TaskDTO;
import com.task.util.EmailUtil;
import com.task.util.Utility;
import com.task.vm.VelocityManager;

public class TaskBizlogic 
{
	
	private static Logger logger = Logger.getLogger(TaskBizlogic.class);

	/**
	 * sendEmailToAll method get list of employee details and send email to
	 * everyone 
	 * @param connection
	 * @throws ParseException 
	 */
	
	public void sendEmailToAll(Connection connection) throws ParseException
	{	
		TaskDAO taskDao = new TaskDAO();
		

		ArrayList <EmpDTO>  employeelist = taskDao.getAllEmployee(connection);
		EmpDTO empDto;
		Date currentDate = new Date();
		
 		for(int i = 0; i < employeelist.size(); i++)
 		{	
 			
			empDto = employeelist.get(i);
			int id = empDto.getId();
			String toAddress = empDto.getEmail();
			empDto.setDate(currentDate);
			String date = empDto.getDate();
			String name = empDto.getName();
			String subject = EmailUtil.getSubject() +" "+ name;
			String body = EmailUtil.getUrl(name, id, date);
			EmailUtil.sendMail(toAddress, subject, body ,EmailUtil.CONTENT_TYPE_TEXT);	
			
		}
    }
	
	/**
	 * saveTaskDetails() method save Task Details of employee in Table
	 * @param taskDto
	 * @param connection
	 */
	
	public void saveTaskDetails(TaskDTO taskDto,Connection connection) 
	{
		TaskDAO taskDao = new TaskDAO();
		taskDao.saveTaskRecord(taskDto,connection);
	}
	
	/** 
	 * 
	 * updateTaskDetails() method call taskDao.updateTaskRecord() method and update existing data of table
	 */
	
	public void updateTaskDetails(TaskDTO taskDto, Connection connection) 
	{
		TaskDAO taskDao = new TaskDAO();
		taskDao.updateTaskRecord(taskDto,connection);		
	}
	
	/**
	 * 
	 * @param id
	 * @param connection
	 * @return
	 */
	public String getEmployeeName(int id, Connection connection) 
	{
		TaskDAO taskDao = new TaskDAO();
		return taskDao.getName(id,connection);
	}
	
	/**
	 * this function send summary of task via email
	 * for sending mail it is call to sendMail() method
	 * @param connection
	 */
	public void sendTaskReport(Connection connection)
	{
		try 
		{
			TaskDAO taskDao = new TaskDAO();
		    ArrayList<TaskDTO> reportList = taskDao.getTaskReportDetails(connection);
		    Calendar calendar = Calendar.getInstance();
		    calendar.add(Calendar.DATE, 0);
			String receiver = EmailUtil.getReceiversAddress();
			int dd = calendar.get(Calendar.DAY_OF_MONTH);
			int mm = calendar.get(Calendar.MONTH);
			int yyyy = calendar.get(Calendar.YEAR);
			mm += 1; //because value of month is start from 0 
			String subject = "Summary of task report for <" +  dd  + "-" + mm
					                                              +"-" +
					                                            yyyy  +">";
			for(int i=0 ; i<reportList.size(); i++)
			{
				TaskDTO dto = reportList.get(i);
				logger.info(dto.getTodaysTask()+" "+dto.getTomorrowsTask());
			}
			
			Map<String,Object> contextMap = new HashMap<String,Object>();
			contextMap.put("reportList", reportList); 
			String emailBody = VelocityManager.getInstance().evaluate(contextMap, "reportEmailTemplete.vm");
			EmailUtil.sendMail(receiver, subject, emailBody, EmailUtil.CONTENT_TYPE_HTML);
			
		}
		catch(Exception e)
		{
			logger.error("Excepton occur at report mail sending time" + e.getMessage(),e);
		}
	}
	
    /**
     * This method return DTO object with existing table record of particular date or null
     * @param id
     * @param date
     * @param connection
     * @return
     */
	
	public TaskDTO getTaskDetailsByDate(int id, Date date, Connection connection) 
	{
		TaskDAO dao = new TaskDAO(); 
		TaskDTO dto = null;
		dto = dao.getRecordByDate(id,connection,date);
		return dto;
	}
    /*
     * return true if data of particular date is exist in database and 
     * otherwise return false.
     */
	public boolean isExist(TaskDTO dto)
	{ 
      boolean exist = false;
  	
  		if(dto != null)
  		{
  			exist = true;
  		}
  		return exist;
	}
	
    /**
     * If application is allow to user for update data then return true
     * otherwise false 
     * @param date
     * @return
     */
	
	public boolean isUpdatable(Date date)
	{
	   boolean updatable = false;
	   Calendar today = Calendar.getInstance();
	   today.add(Calendar.DATE,0);
	   today.set(Calendar.HOUR_OF_DAY, 0);
	   today.set(Calendar.MINUTE, 0);
	   today.set(Calendar.SECOND, 0);
	   today.set(Calendar.MILLISECOND, 0);
	   Calendar parseDate = Calendar.getInstance();
	   parseDate.setTime(date);
		
	   Calendar prevDay = Calendar.getInstance();
	   prevDay.add(Calendar.DATE,-1);
			 
	   logger.info("Todays Date = " + today.get(Calendar.YEAR) + "-" + today.get(Calendar.DAY_OF_MONTH) +"-"+ today.get(Calendar.MONTH) +
				   "\nURL Date = " + parseDate.get(Calendar.YEAR) +"-" + parseDate.get(Calendar.DAY_OF_MONTH) + "-" + 
	                 parseDate.get(Calendar.MONTH));
		                        
	   if(today.compareTo(parseDate) == 0)
	   {
		   updatable = true;
	   }
	   
	   else if(parseDate.get(Calendar.DAY_OF_MONTH) == prevDay.get(Calendar.DAY_OF_MONTH))
	   {
		    
		    Properties properties = new Properties();
			String lastHour = null;
			String serverPath = new File("").getAbsolutePath();
			
			logger.info("serverPath = " + serverPath);
			
			String actualPath = serverPath + "/task_report_prop/TaskManagerContents.properties" ;
			
			logger.info("actualPath = " + actualPath);
			
			try 
			{
				  properties.load( new FileInputStream (new File(actualPath)));
				  lastHour = properties.getProperty ("update.lasthours");
				  
			} 
			catch(FileNotFoundException e)
			{
				logger.error("Propertiey file is not found" + e.getMessage(),e);
			} 
			catch (IOException e) 
			{
				logger.error("Can't fetch the content of property file." + e.getMessage(),e);
			}
			
		    today.set(Calendar.HOUR_OF_DAY,Integer.parseInt(lastHour));
			
			if( (today.get(Calendar.HOUR_OF_DAY)) > (Calendar.getInstance().get(Calendar.HOUR_OF_DAY)))
			{
				updatable = true;
				logger.info("Data is Updatable");
			}
	   }
		 
	   return updatable;
		 
	}
}
