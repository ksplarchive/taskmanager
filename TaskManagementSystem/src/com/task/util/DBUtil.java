package com.task.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException; 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;
/**
 * Singleton Class
 * @author trojen
 *
 */
public class DBUtil 
{	
	private static String URL = null , userName = null , password = null;
	private static DBUtil dbUtil = null;	
	private static final Logger logger = Logger.getLogger(DBUtil.class);
	public static DBUtil getInstance() 
	{	
		if(dbUtil == null) 
		{	
			dbUtil = new DBUtil();
			dbUtil.setDBProperty();	
		}
		
		return dbUtil;
	}
	/**
	 * set database properties using Property file
	 */
    public void setDBProperty() 
    {
    	
		try {	
			
			Properties properties = new Properties();		
			
			String serverPath = new File("").getAbsolutePath();
			
			String actualPath = serverPath + "/task_report_prop/TaskManagerContents.properties" ;
			
			properties.load( new FileInputStream (new File(actualPath)));
			
			URL = "jdbc:mysql://"+properties.getProperty("jdbc.host") +":"+properties.getProperty("jdbc.port")+"/"+
                 properties.getProperty("jdbc.dbname");
			
			userName = properties.getProperty("jdbc.username");
			
			password = properties.getProperty("jdbc.password");
			
			logger.info("Set database properties");
			
		}
		catch(IOException e)
		{
			logger.error("IOException occuer at loading time of DBContent.properties"+e.getMessage(),e);
		}
		
	}

	private static String getConnectionURL()
	{	
		 return URL;
	}
	
	private static String getUserName() 
	{	
		return userName;
	}

	private static String getPassword() 
	{	
		return password;
	}

	private static String getDriver() 
	{	
		return "com.mysql.jdbc.Driver";
	}
	
	public Connection getConnection() 
	{	
		Connection connection = null;
		try 
		{		
		    String connectionURL = getConnectionURL();
		    String username = getUserName();
		    String password = getPassword();
		    final String drivers = getDriver();
	        Class.forName(drivers);
	        connection = DriverManager.getConnection(connectionURL,username,password);
	        
	        logger.info("Connection Successfully created.");
		}  
		catch (ClassNotFoundException e) 
		{
			logger.error(" Making Connection Failed. "+ e.getMessage(),e);
		}
		catch(SQLException e)
		{
			logger.error(" Making Connection Failed. "+e.getMessage(),e);
		}
		return connection;
    }

	public static void closeConnection(Connection connection) 
	{	
		try
		{
			connection.close();
		}
		catch(SQLException e){
			
			logger.error("SQLException occur during the closing connection operation."+e.getMessage(),e);
		}
		
	}
}
