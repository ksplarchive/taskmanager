package com.task.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;

public class Utility 
{
	private static Logger logger = Logger.getLogger(Utility.class);
	
	public Date parseDate(String date , String format)
	{
		Date utilDate = null;
		try 
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			utilDate = dateFormat.parse(date);
		} 
		catch (ParseException e)
		{
			logger.error("Date parsing failed" + e.getMessage(),e);
		}
		
		return utilDate;
	}
	
	

}
