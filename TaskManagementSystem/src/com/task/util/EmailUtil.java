package com.task.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import org.apache.log4j.Logger;

/**
 * 
 * Singleton class
 *
 */

public class EmailUtil 
{
	static String  port=null , socketfactory = null , serveraddress = null , isTLSEnable = null ,
			       authentication = null , mailHost = null ,password = null , sender = null ,serverurl = null, 
			       greeting=null , urlpath=null ,info = null , receiver = null ,subject = null;
	
	private static Logger logger = Logger.getLogger(EmailUtil.class);
	
	public static final String CONTENT_TYPE_HTML = "text/html;charset=utf-8";
	public static final String CONTENT_TYPE_TEXT = "text/plain";
	
	public static void initMail() 
	{	
		EmailUtil mailUtil = new EmailUtil();
		
		if(port == null || socketfactory == null || serveraddress == null || isTLSEnable == null ||subject == null
				 ||  mailHost == null || authentication == null  || password == null || sender == null
				                                                                        ||serverurl == null) 
		{
			mailUtil.setMailProperty();
		}
		
	}
	
	/**
	 * this method set properties to class variable for 
	 */
	
	public void setMailProperty() 
	{
		try
		{
			Properties properties = new Properties();
			
			
			String serverPath = new File("").getAbsolutePath();
			
			logger.info(serverPath);
			
			String actualPath = serverPath + "/task_report_prop/TaskManagerContents.properties" ;
			properties.load( new FileInputStream (new File(actualPath)));
			
			port = properties.getProperty("email.port");
			socketfactory = properties.getProperty("email.socketfactory");
			serveraddress = properties.getProperty("email.serveraddress");
			isTLSEnable = properties.getProperty("email.isTLSEnable");
			authentication = properties.getProperty("email.authentication");
			password = properties.getProperty("email.password");
			mailHost = properties.getProperty("email.mail_host");
			sender = properties.getProperty("email.sender");
			greeting = properties.getProperty("email.greeting");
			info = properties.getProperty("email.info");
			urlpath = properties.getProperty("email.urlpath");
			serverurl = properties.getProperty("email.serverurl");
			receiver = properties.getProperty("email.receiver");
			subject = properties.getProperty("email.subject");
			logger.info("Succefully Fetch Email Contents from properties");
			
		}
		catch(FileNotFoundException e)
		{
			logger.error("Propertiey file is not found" + e.getMessage(),e);
		} 
		catch (IOException e) 
		{
			logger.error("Can't fetch the content of property file." + e.getMessage(),e);
		}
		
	}
	
	/**
	 * this method set Email properties to sendMailToEmployee and sendReport
	 * 
	 */
	
	private static Properties setEmailProperties() 
	{	
		Properties properties = System.getProperties();  
        properties.put(port, 25);  
        properties.put(socketfactory, "25");  
        properties.put(mailHost, serveraddress);  
        properties.put(isTLSEnable, "true");  
        properties.put(authentication, "true");
		return properties;
	}
	
	public static String getUrl(String name,int id,String date)
	{
		return "\n" + greeting + " " + name + ",\n\n" + info + "\n\n" + serverurl + urlpath +  
				                                                              id + "&date=" + date + "\n\nThanks.";
	}
	
	public static void sendMail(String receiverId, String subject ,String body, String contentType) 
	{
		Properties properties = setEmailProperties();  
		Session session =Session.getDefaultInstance(properties,null);
		session.setDebug(true);
		MimeMessage mimeMessage =new MimeMessage(session);
		try 
		{
			InternetAddress toAddress = new InternetAddress(receiverId);
			toAddress = new InternetAddress(receiverId);
			mimeMessage.setFrom(new InternetAddress(sender));	
			mimeMessage.addRecipient(RecipientType.TO, toAddress);
			mimeMessage.setSubject(subject);
			mimeMessage.setContent(body,contentType);
			
			Transport transport = session.getTransport("smtp");
			transport.connect(serveraddress, sender, password);
			transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
			transport.close();
		} 
		catch (AddressException e)
		{
			logger.error("AddressException is occuer during URL sending to client" +
		    e.getMessage(), e);
		} 
		catch (MessagingException e)
		{
			logger.error("MessagingException is occuer during URL sending to client" +
		    e.getMessage(), e);
		}
	}
		
	
	public static String getReceiversAddress() 
	{
		return receiver;
	}

	public static String getSubject() 
	{
		return subject;
	}
	
}
