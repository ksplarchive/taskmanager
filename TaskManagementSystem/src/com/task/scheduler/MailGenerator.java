package com.task.scheduler;

import java.sql.Connection; 
import java.util.Calendar;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.task.bizlogic.TaskBizlogic;
import com.task.util.DBUtil;
import com.task.util.Utility;

public class MailGenerator extends TimerTask 
{
	private Logger logger = Logger.getLogger(MailGenerator.class);
	public void run()
	{	
		try {
			Calendar c = Calendar.getInstance();
			int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			
			if(dayOfWeek != Calendar.SUNDAY && dayOfWeek != Calendar.SATURDAY)
			{
				DBUtil dbUtil = DBUtil.getInstance();
				Connection connection = dbUtil.getConnection();
				TaskBizlogic bizlogic = new TaskBizlogic();
				bizlogic.sendEmailToAll(connection);
				DBUtil.closeConnection(connection);
			}
			else
			{
				logger.info("Mail is Not Sent in week ends.");
			}
		}
		catch(Exception exception)
		{
			logger.error("Exception is occur during the send mails to employee." + 
		                  exception.getMessage(),exception);
		}
	}
}
