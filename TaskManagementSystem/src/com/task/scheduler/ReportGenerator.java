package com.task.scheduler;

import java.sql.Connection;
import java.util.Calendar;

import org.apache.log4j.Logger;

import com.task.bizlogic.TaskBizlogic;
import com.task.util.DBUtil;
import com.task.util.Utility;

public class ReportGenerator implements Runnable {

	Logger logger = Logger.getLogger(ReportGenerator.class);
	@Override
	public void run()
	{
		try {
				Calendar calendar = Calendar.getInstance();
				int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
				
				if(dayOfWeek != Calendar.SUNDAY && dayOfWeek != Calendar.SATURDAY)
				{
			
					DBUtil dbUtil = DBUtil.getInstance();
					Connection connection = dbUtil.getConnection();
			
					TaskBizlogic bizlogic =new TaskBizlogic();
					bizlogic.sendTaskReport(connection);
			
					DBUtil.closeConnection(connection);
					
			    }
				else
				{
					logger.info("Report Mail is Not Sent in week ends.");
				}
		}
		catch(Exception exception)
		{
			logger.error("Exception is occur during the sending report of Employees");
		}
	}
  }
