package com.task.scheduler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import com.task.util.Utility;
/*
 * TaskManagerSchedulerContext class for assign scheduler job to sending email
 * in particular time  
 */
public class TaskManagerSchedulerContext implements ServletContextListener
{
	private static Logger logger = Logger.getLogger(TaskManagerSchedulerContext.class);
	
	public void contextInitialized(ServletContextEvent event)
	{
		String hours = null,minutes = null, hoursForReport = null, minutesForReport = null;
		try
		{
			Properties properties = new Properties();
			
			String serverPath = new File("").getAbsolutePath();
			
			logger.info(serverPath);
			
			String actualPath = serverPath + "/task_report_prop/TaskManagerContents.properties" ;
			
			properties.load( new FileInputStream (new File(actualPath)));
			
			logger.info(actualPath);
			
			hours = properties.getProperty ("time.scheduler.hours");
			minutes = properties.getProperty ("time.scheduler.minutes");
			
			hoursForReport = properties.getProperty ("report.scheduler.hours");
			minutesForReport = properties.getProperty ("report.scheduler.minutes");
			
			logger.info("set scheduler on "+ hours +" : "+minutes +" and " + hoursForReport +" : " +
			             minutesForReport);
			 logger.info("last hour to update " + properties.getProperty ("update.lasthours"));
		}
		catch(FileNotFoundException e)
		{
			logger.error("Properties file is not found" + e.getMessage(),e);
		}
		catch (IOException e) 
		{
			logger.error ("Exception occuer at loading time of TaskManagerContents.properties" + 
		                   e.getMessage(),e);
		}
		
		
		Calendar date = Calendar.getInstance();
		date.add (Calendar.DAY_OF_MONTH, 0);
        date.set(Calendar.HOUR_OF_DAY,Integer.parseInt(hours));
		date.set(Calendar.MINUTE, Integer.parseInt(minutes));
		
		Calendar currentIST = Calendar.getInstance();
		
	    
		if(date.getTimeInMillis() < currentIST.getTimeInMillis())
		{
			date.add (Calendar.DAY_OF_MONTH, 1);
		}
	    
		
		
		ScheduledThreadPoolExecutor exec =   (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(5);
		exec.scheduleWithFixedDelay(new MailGenerator(), date.getTimeInMillis() - currentIST.getTimeInMillis(), 
				                    1000*60*60*24, TimeUnit.MILLISECONDS);
	
	     
		/*
		 * report Scheduler 
		 * 
		 */
        Calendar reportdate = Calendar.getInstance();
       
        reportdate.add (Calendar.DAY_OF_MONTH, 0); 
        reportdate.set(Calendar.HOUR_OF_DAY,Integer.parseInt(hoursForReport));
        reportdate.set(Calendar.MINUTE, Integer.parseInt(minutesForReport));
        
	
		if(reportdate.getTimeInMillis() < currentIST.getTimeInMillis())
		{
			reportdate.add (Calendar.DAY_OF_MONTH, 1);
		}

		ScheduledThreadPoolExecutor execute =   (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(5);
        execute.scheduleWithFixedDelay(new ReportGenerator(), reportdate.getTimeInMillis() - currentIST.getTimeInMillis(),
        		                       1000*60*60*24, TimeUnit.MILLISECONDS);
		
	}
	
	public void contextDestroyed(ServletContextEvent event) 
	{
		
	}
}
