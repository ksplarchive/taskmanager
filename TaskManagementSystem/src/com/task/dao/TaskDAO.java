package com.task.dao;

import java.sql.Connection; 
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.task.dto.EmpDTO;
import com.task.dto.TaskDTO;
import com.task.util.EmailUtil;
import com.task.util.Utility;

public class TaskDAO 
{	
	private static Logger logger = Logger.getLogger(TaskDAO.class);
	
	/**
	 * constructor calls static method initMail() for set all email properties 
	 * if not already set.
	 */
	
	public TaskDAO()
	{	
		EmailUtil.initMail();	
	}

	/**
	 * This method Save the Record in the database
	 * @param taskDto
	 * @param connection
	 */
	
	public  void saveTaskRecord(TaskDTO taskDto ,Connection connection ) 
	{
		
		PreparedStatement preparedStatement=null;
		int empId = taskDto.getId();
		Date date = taskDto.getDate();
		String currentTask = taskDto.getTodaysTask();
		String nextTask= taskDto.getTomorrowsTask();
		try
		{
			
			String query = "INSERT INTO taskdetails(Emp_id,CurrentTask,NextTask,Date) VALUES(?,?,?,?)";
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1,empId);
			preparedStatement.setString(2,currentTask);
			preparedStatement.setString(3,nextTask);
			preparedStatement.setDate(4,new java.sql.Date(date.getTime()));
			preparedStatement.executeUpdate();
			preparedStatement.close();
			
		}
		catch(SQLException e)
		{
			
			logger.error("Exception occuer at data Storing time " + e.getMessage(),e);
		} 
		
	}

	/**
	 * Get all employee  details from database and store it in ArrayList
	 * @param connection
	 * @return ArrayList
	 */
	
	public ArrayList <EmpDTO> getAllEmployee(Connection connection)
	{
		ArrayList<EmpDTO> employeeList = new ArrayList<EmpDTO>(); 
		PreparedStatement preparedStatement=null;
		ResultSet resultSet;
		
	    try
	    {		
	    	String query = "select id,FirstName,LastName,EmailAddress from employee";
			preparedStatement=connection.prepareStatement(query);
			resultSet=preparedStatement.executeQuery();
			
			while(resultSet.next()) 
			{		
				EmpDTO empDto = new EmpDTO();
				int id = resultSet.getInt("id");
				String firstName = resultSet.getString("FirstName");
				String lastName = resultSet.getString("LastName");
				String fullName = firstName+" "+lastName;
				String email = resultSet.getString("EmailAddress");
				empDto.setID(id);
				empDto.setName(fullName);
				empDto.setEmail(email);
				employeeList.add(empDto);
			}
		 preparedStatement.close();
		 logger.info("Get Employees details from database");
		 
		}
		catch(SQLException e)
		{	
			logger.error("SQLException occur at getEmployeeDetails() method" + e.getMessage(),e);
		}
		return employeeList;
	}

	/**
	 * getTaskReportDetails() get summary details from database and send store it in
	 * ArrayList 
	 * @param connection
	 * @return ArrayList
	 */
	
	public ArrayList<TaskDTO> getTaskReportDetails(Connection connection)
	{
		ArrayList <TaskDTO> reportList = new ArrayList<TaskDTO>(); 
		PreparedStatement preparedStatement=null;
		ResultSet resultSet;
		
	    try
		{
	    	
	    	Calendar cal = Calendar.getInstance();
			
	    	int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
			
			if(dayOfWeek == Calendar.MONDAY)
			{
				cal.add(Calendar.DATE, -3); 
			}
			else
			{
				cal.add(Calendar.DATE, -1);
			}
	    	Date date = cal.getTime();
	    	
	    	String query ="select distinct e.id,e.FirstName,e.LastName,t.CurrentTask,t.NextTask from " + 
		                  "employee e left join taskdetails t on e.id=t.Emp_id  AND Date = ?";
			
	    	preparedStatement=connection.prepareStatement(query);
			
			preparedStatement.setDate(1,new java.sql.Date( date.getTime()));
			resultSet=preparedStatement.executeQuery();
			String currentTask;
			String nextTask; 
			
			while(resultSet.next())
			{	
				TaskDTO reportDto = new TaskDTO();
				int id = resultSet.getInt("id");
				String firstName = resultSet.getString("FirstName");
				String lastName = resultSet.getString("LastName");
				currentTask = resultSet.getString("CurrentTask");
				nextTask = resultSet.getString("NextTask");
			
				reportDto.setID(id);
				reportDto.setName(firstName +" "+lastName);
				
				if(currentTask != null)
				{
					reportDto.setTodaysTask(currentTask);
				}
				if(nextTask != null)
				{
					reportDto.setTomorrowsTask(nextTask);
				}
				logger.info(reportDto.getTodaysTask()+reportDto.getTomorrowsTask());
				reportList.add(reportDto);
				
			}
		  
			preparedStatement.close();
		 logger.info("Successfull to get Task details from Employee and TaskDetails Table  ");
		}
		catch(SQLException e)
		{
			logger.error("Exception occur at getEmployeeDetails() method" + e.getMessage(),e);
		}
	    
	    return reportList;
	}
	
	/**
	 * getName() method use to provide names of employee
	 * @param id
	 * @param connection
	 * @return name
	 */

	public String getName(int id, Connection connection) 
	{
		String fullName = null;
		
		try
		{
			String query = "select FirstName,LastName from employee where id = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1, id);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next())
			{
				String firstName = resultSet.getString("FirstName");
				String lastName = resultSet.getString("LastName");
				fullName = firstName+" " + lastName;
			}
			
			logger.info("getting names from db successfully");
			preparedStatement.close();
		}
		catch(SQLException e)
		{
			logger.error("Exception at Employee name fetching operation" + e.getMessage(),e);
		}
		return fullName; 
	}
    /**
     * Get record by Date
     * @param id
     * @param connection
     * @param date
     * @return dto object with data or null
     * @throws ParseException
     */
	
	public TaskDTO getRecordByDate(int id, Connection connection,Date date)
	{

		String currentTask = null;
		String nextTask=null;
		TaskDTO dto = null;
		try {
			
			String query = "select CurrentTask,NextTask from taskdetails where Emp_id = ? AND Date = ?";
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setInt(1,id);
			java.sql.Date s =new java.sql.Date(date.getTime());
			System.out.println(s);
			preparedStatement.setDate(2,new java.sql.Date(date.getTime()));
			ResultSet resultSet = preparedStatement.executeQuery();
			
			while(resultSet.next())
			{
				dto = new TaskDTO();
				currentTask = resultSet.getString("CurrentTask");
				nextTask = resultSet.getString("NextTask");
				dto.setTodaysTask(currentTask);
				dto.setTomorrowsTask(nextTask);
				
			}
			preparedStatement.close();
		}
		catch(SQLException e)
		{
			logger.error("Failed to get last record of task details by Date." + e.getMessage(),e);
		}
		return dto;
	}	
	/**
	 * This update the record of Database
	 * @param taskDto
	 * @param connection
	 */
	public  void updateTaskRecord(TaskDTO taskDto ,Connection connection ) 
	{
		
		PreparedStatement preparedStatement=null;
		int emp_id = taskDto.getId();
		Date date = taskDto.getDate();
		String currentTask = taskDto.getTodaysTask();
		String nextTask= taskDto.getTomorrowsTask();
		try
		{
			String query = "UPDATE taskdetails Set CurrentTask = ?, NextTask = ? WHERE Emp_id = ? AND Date = ?";
			preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1,currentTask);
			preparedStatement.setString(2,nextTask);
			preparedStatement.setInt(3,emp_id);
			preparedStatement.setDate(4,new java.sql.Date( date.getTime()));
			preparedStatement.executeUpdate();
			preparedStatement.close();
		}
		catch(SQLException e)
		{
			logger.error("Exception at data Updation time." + e.getMessage(),e);
		} 
		
	}
}
