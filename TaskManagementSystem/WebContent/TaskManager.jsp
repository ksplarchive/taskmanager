<%-- 
  - Description:
     TaskManager.jsp file is Welcome page of task reporter app
     which is display name of employee with date and if any already entered 
     current task and next task information.
--%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- HTML page with JSTL tags -->
<html>
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Task Details</title>
	
	<!-- External JS files for add and edit task related data -->
	<script src="jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="taskmanager.js"> </script>
  
  </head>
  <!-- main body -->
  <body>
  <!-- include header file of page. -->
  
	<c:import url="Header.jsp"></c:import>
	<div align="center">
	  <!-- form -->
      <form id="form1"  name ="TaskStoreServlet" style="width: 800px">
	    
	    <fieldset>
	     
	      
	      <h3 style="color: gray;" name = "name">Welcome  <c:out value="${taskDetailsMap.name}"></c:out>,</h3>
	      <h3 style="color: gray;" name = "date"> Your task details for <c:out value="${taskDetailsMap.date}"></c:out></h3>
	    
	      <input type="hidden" id="emp_id" name="id" value="<c:out value="${taskDetailsMap.id}"></c:out>" > 
	      <input type="hidden" id="date" name="date" value="<c:out value="${taskDetailsMap.date}"></c:out>">
	      
	       <!-- hidden div tag visible after database operation  -->
	      <div id="result" style ="display: none" >
	    	<h3 style="color: green;" name="name">Your task details are saved successfully, 
	    	                                                     <img src = "big-smiley-001.gif" /> </h3>
	      </div>
	      <table>
		    <tr style="height:10px"> <td></td> </tr>
		    <tr style="height:10px"> <td></td> </tr>
		    <tr style="height:10px"> <td></td> </tr>
		  
		    <tr>
		      <td valign="top">Today's Task </td>
		      <td style="height:10px"></td>
		      <td>
		        <textarea id = "today_id"rows="12" 
		         cols="70" name="todays_task" spellcheck="true" title="What you done today?"><c:out value="${taskDetailsMap.currentTask}"></c:out></textarea> 
		      </td>
		    </tr>
		  
		    <tr style="height:10px"><td></td></tr>
		      <tr> 
		        <td valign="top">Tomorrow's Task </td>
		        <td style="height:10px"></td>
		        <td> 
		          <textarea id="tomorrow_id" rows="12" 
		           cols = "70" name="tomorrows_task" title = "What is your Tomorrows Task ?" ><c:out value ="${taskDetailsMap.nextTask}"></c:out></textarea> 
		        </td>
		      </tr>
		  
		    <tr style="height:10px"><td></td></tr>
		    <tr>
		      <td align="right">
		       <c:if test="${taskDetailsMap.exist == false}">  
		       		        
		        <input type="button" onClick = "javascript:sendInfo(<c:out value ="${taskDetailsMap.exist}"></c:out>,<c:out value ="${taskDetailsMap.isUpdatable}"></c:out>)" id="insert" value="Submit" >
		           
		     </c:if>
		      </td>
			  <td style="height:10px"></td>
			  <td align="left">
			    <input  type="reset"> 
			    
			   <c:if test="${taskDetailsMap.exist == true}"> 
			  	   <input type="button"  onClick = "javascript:sendInfo(<c:out value ="${taskDetailsMap.exist}"></c:out>,<c:out value ="${taskDetailsMap.isUpdatable}"></c:out>)" id="update" value="Update" >
			   </c:if>
			  </td>
			  
			</tr>
		  </table>
	    </fieldset>
	  </form>
	</div>
  </body>
</html>