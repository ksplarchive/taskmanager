function sendInfo(status , send) 
{
	if(!send)
	{
		alert("Sorry ,data is not saved in Database, please check date.");
		return null;
	}
    var JSONobj = new Object();
    JSONobj.id = $('#emp_id').val();
    JSONobj.date=$('#date').val();
    JSONobj.current_task = $('#today_id').val();
    JSONobj.next_task = $('#tomorrow_id').val();
    if(!status)
    {
    	JSONobj.status = "insert";
    }
    else
    {
    	JSONobj.status = "update";
    }
    
	$.ajax({
	   	  url:'TaskStoreServlet',
	      datatype:'json',
	      type: 'POST',
	      data : JSON.stringify(JSONobj),
	      success:function(data){
	    	        
	                $('#result').css("display", "block");
	              },
	      error:function(data) 
	            {
	    	      alert("error at  sending time" + data);
	            }
	      });   	   
}